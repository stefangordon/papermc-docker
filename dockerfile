FROM debian:10.2-slim
ARG Bedrock_Version=1.13.3.0

ENV VERSION=$Bedrock_Version

RUN apt-get update && \
    apt-get install -y unzip curl libcurl4 && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /app/bedrock && \
    mkdir -p /app/config && \
    mkdir -p /app/worlds

RUN curl https://minecraft.azureedge.net/bin-linux/bedrock-server-${VERSION}.zip --output bedrock.zip && \
    unzip bedrock.zip -d /app/bedrock && \
    rm bedrock.zip

RUN mkdir -p /app/bedrock/config && \
    cp /app/bedrock/server.properties /app/bedrock/config/server.properties && \
    cp /app/bedrock/permissions.json /app/bedrock/config/permissions.json && \
    cp /app/bedrock/whitelist.json /app/bedrock/config/whitelist.json && \
    rm /app/bedrock/server.properties && \
    rm /app/bedrock/permissions.json && \
    rm /app/bedrock/whitelist.json && \
    ln -s /app/worlds /app/bedrock/worlds

COPY docker-entrypoint.sh /app/
RUN chmod 755 /app/docker-entrypoint.sh

EXPOSE 19132/udp

VOLUME ["/app/config", "/app/worlds"]

WORKDIR /app
ENV LD_LIBRARY_PATH=/app/bedrock
ENTRYPOINT [ "/app/docker-entrypoint.sh" ]