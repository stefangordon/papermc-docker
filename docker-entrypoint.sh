#!/bin/bash

echo "Checking for config files in mounted volumes..."
if [ ! -f /app/config/server.properties ] ; then
    echo "Config files missing. Copying defaults..."
    mv /app/bedrock/config/server.properties /app/config/ && \
    mv /app/bedrock/config/permissions.json /app/config/ && \
    mv /app/bedrock/config/whitelist.json /app/config/
fi

rm /app/bedrock/server.properties 2> /dev/null
rm /app/bedrock/whitelist.json 2> /dev/null
rm /app/bedrock/permissions.json 2> /dev/null
ln -s /app/config/server.properties /app/bedrock/server.properties && \
ln -s /app/config/permissions.json /app/bedrock/permissions.json && \
ln -s /app/config/whitelist.json /app/bedrock/whitelist.json

cd /app/bedrock
./bedrock_server
